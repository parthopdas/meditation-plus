# Meditation+ App

![Screenshot of meditation tab](https://raw.githubusercontent.com/Sirimangalo/meditation-plus-angular/master/src/assets/img/screenshot.jpg)
![Screenshot of doing meditation](https://raw.githubusercontent.com/Sirimangalo/meditation-plus-angular/master/src/assets/img/screenshot2.jpg)
![Screenshot of profile](https://raw.githubusercontent.com/Sirimangalo/meditation-plus-angular/master/src/assets/img/screenshot3.jpg)

## Client

Angular Client for Meditation+ REST API.

### Configuration

Add a `client/src/api.config.ts`:

```js
export let ApiConfig = {
  url: 'http://localhost:3002' // the REST endpoint
};
```

### Install dependencies and run

```bash
# inside "client" of the cloned repository
$ cd client

# install dependencies
$ yarn

# run client
$ yarn run start
```

### Testing Development Tips
To isolate a spec file per run, change the function name `it` to `fit` or `describe` to `fdescribe`.

## Server

REST API built with Node, Express, PassportJS, MongoDB, Mongoose and Socket.io.

### Quick Start

All paths are relative to the `server/` directory!

1. Install dependencies:

```bash
yarn install
```

2. Add a minimal `src/config.config.json`:

```json
{
  "HOST" : "http://localhost:4200",
  "ENV" : "development",
  "PORT" : 3002,
  "MONGO_URI" : {
    "DEVELOPMENT" : "mongodb://localhost:27017/meditation-plus-dev",
    "PRODUCTION" : "mongodb://localhost:27017/meditation-plus-prod",
    "TEST" : "mongodb://localhost:27017/meditation-plus-test"
  },
  "SESSION_SECRET" : "cats",
  "GOOGLE_API_KEY" : "",
  "FIREBASE_DB": "",
  "MESSAGING_APPOINTMENT_TOPIC": "",
  "UPLOAD_DIR": "",
  "PUBLIC_DIR": "",
  "DISCORD_CHANNEL_ID": "",
  "DISCORD_BOT_TOKEN": ""
}
```

**Notes:** 

  - In the client configuration the same port should be used (in `client/src/api.config.ts`).
  - You need MongoDB [installed](https://docs.mongodb.com/manual/installation/) and running for it to work! A good GUI tool in case you need to manipulate the database is [Robo3T](https://robomongo.org/download). 

3. Start server:

```bash
yarn start
```

4. (Optional) Populate database with developement data:

```bash
cd src/scripts
node src/scripts/populate-runner.js
```

This useful script creates some example data, i.e. some users like `admin`, `kelly`, `tommy` (all with the password `password`). This saves you time registering a new account and having to manually set the `verified` property of the new user to `true` in the database.

### Additional configuration

For some features to work properly you need to further modify the `config.json`:

#### Uploading avatar images

Please create 2 new folders and set `UPLOAD_DIR` (temporary directory for uploading images) and `PUBLIC_DIR` (public directory for serving images) to their absolute path.

#### YouTube suggestions while typing questions

Please create a Google API key with activated Youtube Data v3 API and set the `GOOGLE_API_KEY` property to it.

#### Push notifications

For push notifications (i.e. for chat messages) to work locally, you need to first create a new [firebase](https://firebase.google.com/) project and then

- set `FIREBASE_DB` to the url which looks like `https://myproject.firebaseio.com`
- download the private server config and move it to `src/config/firebase.json`

**Note:** There might arise issues if the local app does not have `https://`

#### Using Discord for appointments

For this to work, you need to set

- **DISCORD_CHANNEL_ID:** The id of a discord channel where meditators should get invited to on appointments (if they choose to use discord).
- **DISCORD_BOT_TOKEN:** A token of the discord bot who is on the same server as the channel from above and has permissions to create invite links for it.

and optionally

- **MESSAGING_APPOINTMENT_TOPIC:** A token used to send a push message to [another app](https://gitlab.com/sirimangalo/meditation-plus-android) in order to trigger ringing notification on Ven. Yuttadhammo Bhikkhu's phone for appointments. Use "staging" if you have downloaded the app from an artifact of a master build (not a tag pipeline).

### API Documentation

You can view the latest API Documention in the [Build artifacts](https://gitlab.com/sirimangalo/meditation-plus/-/jobs/artifacts/master/file/server/doc/index.html?job=build:dev).

