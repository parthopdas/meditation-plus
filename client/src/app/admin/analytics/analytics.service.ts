import { Injectable } from '@angular/core';
import { ApiConfig } from '../../../api.config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AnalyticsService {

  public constructor(
    private http: HttpClient
  ) {
  }

  public getUserStats() {
    return this.http.get(
      ApiConfig.url + '/api/analytics-users'
    );
  }

  public getSignupStats(minDate = null, interval = null, format = null): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/analytics-signups',
      JSON.stringify({ minDate: minDate, interval: interval, format: format })
    );
  }

  public getMeditationStats(minDate = null, interval = null, format = null): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/api/analytics-meditations',
      JSON.stringify({ minDate: minDate, interval: interval, format: format })
    );
  }

  public getCountryStats(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/analytics-countries');
  }

  public getTimezoneStats(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/analytics-timezones');
  }
}
