import { Component } from '@angular/core';
import { BroadcastService } from './broadcast.service';
import { SettingsService } from '../../shared/settings.service';
import { tap, concatMap, filter } from 'rxjs/operators';
import { DialogService } from '../../dialog/dialog.service';
import { MatSnackBar } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { ThrowError } from '../../actions/global.actions';

@Component({
  selector: 'broadcast-admin',
  templateUrl: './broadcast-admin.component.html',
  styleUrls: [
    './broadcast-admin.component.styl'
  ]
})
export class BroadcastAdminComponent {

  loadingSettings: boolean;
  loadingBroadcasts: boolean;
  loadingSubmit: boolean;

  // broadcast data
  broadcasts: any[] = [];
  activeBroadcast;

  livestreamInfo: string;

  constructor(
    private broadcastService: BroadcastService,
    private settingsService: SettingsService,
    private dialog: DialogService,
    private snackbar: MatSnackBar,
    private store: Store<AppState>
  ) {
    this.loadingSettings = true;
    this.loadBroadcasts();
    this.loadSettings();
  }

  findActiveBroadcast(broadcasts: any[]) {
    for (const broadcast of broadcasts) {
      if (broadcast.started && !broadcast.ended) {
        this.activeBroadcast = broadcast;
        return;
      }
    }
  }

  /**
   * Loads all broadcasts
   */
  loadBroadcasts() {
    this.activeBroadcast = null;
    this.broadcastService
      .getAll()
      .pipe(
        tap(res => this.findActiveBroadcast(res))
      )
      .subscribe(res => {
        this.broadcasts = res;
        this.loadingBroadcasts = false;
      });
  }

  /**
   * Loads settings entity (needed for livestream info text)
   */
  loadSettings() {
    this.settingsService.get()
      .subscribe(settings => {
        this.loadingSettings = false;
        this.livestreamInfo = settings.livestreamInfo
          ? settings.livestreamInfo
          : this.livestreamInfo;
      });
  }

  updateLivestreamInfo(evt = null) {
    this.loadingSubmit = true;

    if (evt) {
      evt.preventDefault();
    }

    this.settingsService.set('livestreamInfo', this.livestreamInfo)
      .subscribe(
        () => {
          this.snackbar.open('Info has been updated successfully.');
          this.loadSettings();
        },
        error => this.store.dispatch(new ThrowError({ error })),
        () => this.loadingSubmit = false
      );
  }

  delete(evt, broadcast) {
    evt.preventDefault();

    this.dialog.confirmDelete().pipe(
      filter(val => !!val),
      concatMap(() => this.broadcastService.delete(broadcast))
    ).subscribe(() => this.loadBroadcasts());
  }
}
