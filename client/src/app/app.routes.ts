import { Routes } from '@angular/router';
import { LoginComponent } from './login';
import { NotFoundComponent } from './not-found';
import { ProfileComponent, ProfileFormComponent } from './profile';
import { LiveComponent } from './live';
import { OnlineComponent } from './online';
import { CommitmentComponent } from './commitment';
import { UpdateComponent } from './update';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AuthGuard } from './shared/auth-guard';
import { LoginGuard } from './shared/login-guard';
import { AdminGuard } from './shared/admin-guard';

export const ROUTES: Routes = [
  {
    path: 'home',
    canLoad: [AuthGuard],
    loadChildren: './home/home.module#HomeModule'
  },
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'profile', component: ProfileFormComponent, canActivate: [AuthGuard] },
  { path: 'profile/id/:id', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'profile/:username', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'online', component: OnlineComponent, canActivate: [AuthGuard] },
  { path: 'commit', component: CommitmentComponent, canActivate: [AuthGuard] },
  { path: 'live', component: LiveComponent, canActivate: [AuthGuard] },
  { path: 'updated', component: UpdateComponent },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  {
    path: 'admin',
    canLoad: [AdminGuard],
    loadChildren: './admin/admin.module#AdminModule'
  },
  {
    path: 'schedule',
    canLoad: [AuthGuard],
    loadChildren: './appointment/appointment.module#AppointmentModule'
  },
  { path: 'meet', redirectTo: 'schedule', pathMatch: 'full' },
  {
    path: 'testimonials',
    canLoad: [AuthGuard],
    loadChildren: './testimonial/testimonial.module#TestimonialModule'
  },
  { path: 'reset-password', component: ResetPasswordComponent, canActivate: [LoginGuard] },
  { path: '**', component: NotFoundComponent }
];
