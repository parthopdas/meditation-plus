import { Action } from '@ngrx/store';
import { Meeting } from '../appointment';

export const LOAD_MEETING = '[Appointment] Load Meeting';
export const LOAD_MEETING_DONE = '[Appointment] Load Meeting Done';
export const INITIATE_MEETING = '[Appointment] Initiate Meeting';
export const INITIATE_MEETING_ERROR = '[Appointment] Initiate Meeting Error';
export const INITIATE_MEETING_DONE = '[Appointment] Initiate Meeting Done';

export class LoadMeeting implements Action {
  readonly type = LOAD_MEETING;
}

export class LoadMeetingDone implements Action {
  readonly type = LOAD_MEETING_DONE;
  constructor(public payload: Meeting) {}
}

export class InitiateMeeting implements Action {
  readonly type = INITIATE_MEETING;
}

export class InitiateMeetingDone implements Action {
  readonly type = INITIATE_MEETING_DONE;
}

export class InitiateMeetingError implements Action {
  readonly type = INITIATE_MEETING_ERROR;
  constructor(public payload: string) {}
}

export type Actions = LoadMeeting | LoadMeetingDone | InitiateMeeting | InitiateMeetingDone | InitiateMeetingError;
