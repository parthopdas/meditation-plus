export interface Appointment {
  _id: string;
  user?: any;
  hour: number;
  weekDay: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface Meeting {
  _id: string;
  user: any;
  appointment: Appointment;
  initiated: boolean;
  discordLink?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface AppointmentHourVO {
  status?: null | 'editing' | 'loading' | 'editLoading' | 'success' | 'error';
  errorMessage?: string;
  days?: any;
}
