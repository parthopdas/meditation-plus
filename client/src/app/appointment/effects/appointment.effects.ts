import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { concatMap, map, catchError } from 'rxjs/operators';
import { AppointmentService } from '../appointment.service';
import {
  LOAD_MEETING,
  LoadMeeting,
  LoadMeetingDone,
  INITIATE_MEETING,
  InitiateMeeting,
  InitiateMeetingDone,
  InitiateMeetingError
} from '../actions/appointment.actions';
import { of } from 'rxjs';
import { ThrowError } from 'app/actions/global.actions';

@Injectable()
export class AppointmentEffect {
  constructor(
    private actions$: Actions,
    private service: AppointmentService
  ) {
  }

  @Effect()
  loadMeeting$ = this.actions$
    .ofType<LoadMeeting>(LOAD_MEETING)
    .pipe(
      concatMap(() =>
        this.service.getMeeting().pipe(
          map(meeting => new LoadMeetingDone(meeting)),
          catchError(error => {
            if (error.status === 403) {
              return of();
            }

            return of(new ThrowError({ error }));
          })
        )
      )
    );

  @Effect()
  post$ = this.actions$
    .ofType<InitiateMeeting>(INITIATE_MEETING)
    .pipe(
      concatMap(() =>
        this.service.meetingNotify().pipe(
          map(() => new InitiateMeetingDone()),
          catchError(({ error }) => of(new InitiateMeetingError(error.error || 'An unknown error occurred.')))
        )
      )
    );
}
