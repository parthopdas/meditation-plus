import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { InputDialogComponent } from './input-dialog/input-dialog.component';
import { Observable } from 'rxjs';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { map } from 'rxjs/operators';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { AvatarUploadDialogComponent } from './avatar-upload-dialog/avatar-upload-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog: MatDialog) { }

  /**
   * Displays a dialog with an input field.
   *
   * @param title Dialog title
   * @param text Dialog text
   * @param placeholder Placeholder for the input field
   * @param initialData Initial data for the input field
   * @returns Input field's data or `undefined` when cancelled as an Observable.
   */
  input(
    title: string,
    text: string,
    placeholder = 'Your data',
    initialData = ''
  ): Observable<string> {
    const dialog = this.dialog.open(InputDialogComponent, {
      data: { title, text, placeholder, initialData }
    });
    return dialog.afterClosed();
  }

  /**
   * Displays a dialog with buttons YES and NO.
   *
   * @param title Dialog title
   * @param text Dialog text
   * @returns `true` for YES and `false` for NO or when cancelled as an Observable.
   */
  confirm(title: string, text: string): Observable<boolean> {
    const dialog = this.dialog.open(ConfirmDialogComponent, {
      data: { title, text }
    });
    return dialog.afterClosed().pipe(
      map(val => !!val)
    );
  }

  /**
   * Shortcut for a deletion confirmation dialog.
   */
  confirmDelete(): Observable<boolean> {
    return this.confirm(
      'Confirmation',
      'Are you sure you want to delete this entry?'
    );
  }

  /**
   * Displays a dialog.
   *
   * @param title Dialog title
   * @param text Dialog text
   */
  alert(title: string, text: string): Observable<void> {
    const dialog = this.dialog.open(AlertDialogComponent, {
      data: { title, text }
    });
    return dialog.afterClosed();
  }

  uploadAvatar(userId: string = ''): Observable<String> {
    const dialog = this.dialog.open(AvatarUploadDialogComponent, {
      data: { userId }
    });
    return dialog.afterClosed();
  }
}
