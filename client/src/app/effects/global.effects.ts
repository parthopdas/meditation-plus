import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { tap } from 'rxjs/operators';
import { ErrorHandlerService } from '../shared/error-handler.service';
import { ThrowError, THROW_ERROR } from '../actions/global.actions';

@Injectable()
export class GlobalEffect {
  constructor(
    private actions$: Actions,
    private errorHandler: ErrorHandlerService
  ) {
  }

  @Effect({ dispatch: false })
  login$ = this.actions$
    .ofType<ThrowError>(THROW_ERROR)
    .pipe(
      tap(action => this.errorHandler.handle(action.payload.message, action.payload.error))
    );

}
