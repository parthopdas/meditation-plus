import { NgModule } from '@angular/core';
import { HomeComponent } from '.';
import { RouterModule, Routes } from '@angular/router';
import { MeditationModule } from '../meditation';
import { QuestionModule } from '../question';
import { SharedModule } from '../shared';
import { MessageModule } from '../message';

const routes: Routes = [
  { path: '', component: HomeComponent }
];

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    MessageModule,
    MeditationModule,
    QuestionModule
  ],
})
export class HomeModule { }
