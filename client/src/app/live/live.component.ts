import { Component, OnDestroy } from '@angular/core';
import { LiveService } from './live.service';
import { SettingsService } from '../shared/settings.service';
import { concatMap, retry } from 'rxjs/operators';
import { interval } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { SetTitle } from '../actions/global.actions';

@Component({
  selector: 'live',
  templateUrl: './live.component.html',
  styleUrls: [
    './live.component.styl'
  ]
})
export class LiveComponent implements OnDestroy {

  liveStream;
  intervalSubscription;

  loadingSettings: boolean;
  settings;

  constructor(
    private liveService: LiveService,
    private settingsService: SettingsService,
    store: Store<AppState>
  ) {
    store.dispatch(new SetTitle(''));

    this.loadingSettings = true;
    this.settingsService.get()
      .subscribe(res => {
        this.loadingSettings = false;
        this.settings = res;
      });

    this.intervalSubscription = interval(10000)
      .pipe(
        concatMap(() => this.liveService.getLiveData()),
        retry()
      )
      .subscribe(res => this.liveStream = res);

    liveService.getLiveData()
      .subscribe(res => this.liveStream = res);
  }

  ngOnDestroy() {
    this.intervalSubscription.unsubscribe();
  }
}
