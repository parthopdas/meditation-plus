import { MaterialModule } from '../shared/material.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { UserService } from '../user/user.service';
import { FakeUserService } from '../user/testing/fake-user.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockStore } from 'testing/mock.store';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { MockActions } from 'testing/mock.actions';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LoginComponent
      ],
      imports: [
        MaterialModule,
        NoopAnimationsModule,
        FormsModule,
        RouterTestingModule,
        SharedModule,
        HttpClientTestingModule
      ],
      providers: [
        {provide: UserService, useClass: FakeUserService},
        {provide: Store, useClass: MockStore},
        {provide: Actions, useClass: MockActions}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
