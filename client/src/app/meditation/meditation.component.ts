import { Observable, interval } from 'rxjs';
import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { CommitmentService } from '../commitment/commitment.service';
import * as moment from 'moment';
import * as StableInterval from 'stable-interval';
import { filter, retry, take, withLatestFrom, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectProfile } from '../auth/reducders/auth.reducers';
import { MatBottomSheet } from '@angular/material';
import { StartMeditationComponent } from './start-meditation/start-meditation.component';
import { Meditation } from './meditation';
import * as meditation from './reducers/meditation.reducers';
import { LoadMeditations, LikeMeditations } from './actions/meditation.actions';
import { MeditationEffect } from './effects/meditation.effect';
import { Profile } from '../profile/profile';

/**
 * Component for the meditation tab inside home.
 */
@Component({
  selector: 'meditation',
  templateUrl: './meditation.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: [
    './meditation.component.styl'
  ]
})
export class MeditationComponent implements OnInit, OnDestroy {
  keys = Object.keys;

  @Output() loadingFinished: EventEmitter<any> = new EventEmitter<any>();

  // url for static audio file
  bell;
  bellInterval;

  // meditation data
  activeMeditations$: Observable<Meditation[]>;
  finishedMeditations$: Observable<Meditation[]>;

  ownSession$: Observable<Meditation | null>;
  loadedInitially$: Observable<boolean>;
  lastUpdated$: Observable<moment.Moment | null>;
  lastMeditationSession$: Observable<moment.Moment | null>;
  profile$: Observable<Profile>;
  liking$: Observable<boolean>;

  meditationSubscription;
  meditationSocket;

  // subscribed commitments
  commitments;

  pollMeditationsInterval;

  constructor(
    private commitmentService: CommitmentService,
    private store: Store<AppState>,
    private bottomSheet: MatBottomSheet,
    private effect: MeditationEffect,
    private chdet: ChangeDetectorRef
  ) {
    this.activeMeditations$ = this.store.select(meditation.selectActive);
    this.finishedMeditations$ = this.store.select(meditation.selectFinished);
    this.lastMeditationSession$ = this.store.select(meditation.selectLastMeditationSession);
    this.lastUpdated$ = this.store.select(meditation.selectLastUpdated);
    this.loadedInitially$ = this.store.select(meditation.selectInitiallyLoaded);
    this.ownSession$ = this.store.select(meditation.selectOwnSession);
    this.profile$ = this.store.select(selectProfile);
    this.liking$ = this.store.select(meditation.selectLiking);
    this.effect.cancelDone$.subscribe(() => this.stopTimer());
  }

  loadMeditations() {
    this.store.dispatch(new LoadMeditations());
  }

  /**
   * Start polling observable
   */
  pollMeditations() {
    this.pollMeditationsInterval = interval(10000).pipe(
      map(() => null),
      withLatestFrom(this.lastUpdated$),
      filter(([any, lastUpdated]) => {
        if (!lastUpdated) {
          return true;
        }

        // only update when a minute is reached
        const duration = moment.duration(moment().diff(lastUpdated));
        return duration.asMinutes() > 1;
      }),
      retry(),
    ).subscribe(() => {
      this.store.dispatch(new LoadMeditations());
    });
  }

  getBellUrl(profile: Profile, walking: number, sitting: number): string {
    const bellName = profile.sound.replace(/\/assets\/audio\/|.mp3/g, '');
    let bellUrl = 'https://share.sirimangalo.org/static/sounds/' + bellName + '/';

    if (!walking || !sitting) {
      bellUrl += (walking ? walking : sitting);
    } else {
      bellUrl += walking + '_' + sitting;
    }

    return bellUrl;
  }

  /**
   * Method for liking meditation sessions of other users.
   */
  like() {
    this.store.dispatch(new LikeMeditations());
  }

  /**
   * Event which gets triggered by user after submitting a meditation.
   */
  startTimer(session: Meditation) {
    // setup timer & bell,
    // prevent 'gesture-requirement-for-media-playback' on mobile browsers
    this.bell = new Audio();
    this.bell.src = '/assets/audio/halfsec.mp3';
    this.bell.play();

    // check for Network API support
    const connection = window.navigator['connection']
      || window.navigator['mozConnection']
      || null;

    this.profile$.pipe(
      take(1),
    ).subscribe(profile => {
      // use the more stable HTML5 <audio> solution for playing a bell
      // if the user has activated this feature or we know that he isn't
      // using a cellular connection.
      if (profile && profile.stableBell ||
        connection && connection.type && connection.type !== 'cellular') {
        // wait for 'halfsec.mp3'
        setTimeout(() => this.stableTimer(profile, session.walkingLeft, session.sittingLeft), 700);
      } else {
        this.fallbackTimer(profile, session.walkingLeft, session.sittingLeft);
      }
    });
  }

  fallbackTimer(profile: Profile, walking = 0, sitting = 0): void {
    if (!profile || !profile.sound || walking + sitting <= 0) {
      return;
    }

    if (!this.bell) {
      this.bell = new Audio();
    }

    this.bell.src = profile.sound;
    this.bell.onerror = () => console.error('cannot play bell');

    const walkingDone = walking > 0 ? moment().add(walking, 'minutes') : null;
    const sittingDone = sitting > 0 ? moment().add(walking + sitting, 'minutes') : null;

    this.bellInterval = new StableInterval();
    this.bellInterval.set(() => {
      if (moment().isAfter(sittingDone, 'minute')) {
        this.bellInterval.clear();
      }

      if (walkingDone && moment().isSame(walkingDone, 'minute')) {
        this.bell.currentTime = 0;
        this.bell.play();
      }

      if (walkingDone && moment().isSame(sittingDone, 'minute')) {
        this.bell.currentTime = 0;
        this.bell.play();
      }
    }, 60000);
  }

  stableTimer(profile: Profile, walking = 0, sitting = 0): void {
    if (!profile || !profile.sound || walking + sitting <= 0) {
      return;
    }

    if (!this.bell) {
      this.bell = new Audio();
    }

    this.bell.onerror = () => {
      if (new RegExp('\.ogg$').test(this.bell.src)) {
        this.bell.src = this.getBellUrl(profile, walking, sitting) + '.m4a';
        this.bell.currentTime = 0;
        this.bell.play();
      } else {
        this.fallbackTimer(profile, walking, sitting);
      }
    };

    this.bell.src = this.getBellUrl(profile, walking, sitting) + '.ogg';
    this.bell.currentTime = 0;
    this.bell.play();
  }

  stopTimer() {
    if (this.bell) {
      this.bell.pause();
    }

    if (this.bellInterval) {
      this.bellInterval.clear();
    }
  }

  /**
   * Loads subscribed commitments with progress
   */
  loadSubscribedCommitments() {
    this.commitmentService
      .getCurrentUser()
      .subscribe(res => {
        this.commitments = res;
        this.chdet.markForCheck();
      });
  }

  openSheet() {
    const sheetRef = this.bottomSheet.open(StartMeditationComponent);
    this.effect.postDone$.subscribe(() => sheetRef.dismiss());
    this.ownSession$.pipe(
      filter(session => !!session),
      take(1)
    ).subscribe(session => this.startTimer(session));
  }

  ngOnInit() {
    this.store.dispatch(new LoadMeditations());

    // load subscribed commitments
    this.loadSubscribedCommitments();

    this.pollMeditations();
    this.loadingFinished.emit();
  }

  ngOnDestroy() {
    if (this.bellInterval) {
      this.bellInterval.clear();
    }

    if (this.pollMeditationsInterval) {
      this.pollMeditationsInterval.unsubscribe();
    }
  }
}
