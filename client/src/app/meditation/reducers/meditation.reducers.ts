import * as meditation from '../actions/meditation.actions';
import * as moment from 'moment';
import * as _ from 'lodash';
import { AppState } from '../../reducers';
import { createSelector } from '@ngrx/store';
import { Meditation } from '../meditation';

export interface MeditationState {
  active: Meditation[];
  finished: Meditation[];
  loading: boolean;
  posting: boolean;
  liking: boolean;
  loadedInitially: boolean;
  lastUpdated: moment.Moment | null;
  lastMeditationSession: moment.Moment | null;
  ownSession: Meditation | null;
}

export const initialMeditationState: MeditationState = {
  active: [],
  finished: [],
  loading: false,
  posting: false,
  liking: false,
  loadedInitially: false,
  lastUpdated: null,
  lastMeditationSession: null,
  ownSession: null
};

export function meditationReducer(
  state = initialMeditationState,
  action: meditation.Actions
): MeditationState {
  switch (action.type) {
    case meditation.LOAD: {
      return {
        ..._.cloneDeep(state),
        loading: true
      };
    }

    case meditation.LOAD_DONE: {
      return loadDone(state, action);
    }

    case meditation.LOAD_ERROR: {
      return {..._.cloneDeep(state), loading: false};
    }

    case meditation.POST: {
      return {..._.cloneDeep(state), posting: true};
    }

    case meditation.POST_DONE: {
      return {..._.cloneDeep(state), posting: false};
    }

    case meditation.LIKE: {
      return {..._.cloneDeep(state), liking: true};
    }

    case meditation.LIKE_DONE: {
      return {..._.cloneDeep(state), liking: false};
    }

    default: {
      return state;
    }
  }
}

// Extracted more complex reducer functions

function loadDone(state: MeditationState, action: meditation.LoadMeditationsDone): MeditationState {
  let lastMeditationSession = null;

  action.payload.meditations.forEach(data => {
    // updated latest meditation session date
    if (!lastMeditationSession || moment(data.createdAt) > lastMeditationSession) {
      lastMeditationSession = moment(data.createdAt);
    }
  });

  const active = action.payload.meditations.filter(data => data.sittingLeft + data.walkingLeft > 0);
  const finished = action.payload.meditations.filter(data => data.sittingLeft + data.walkingLeft === 0);

  const ownSession = active.find(data => data.user._id === action.payload.currentUserId);

  return {
    ..._.cloneDeep(state),
    active: active.sort(sortMeditations),
    finished: finished.sort(sortMeditations),
    loading: false,
    lastUpdated: moment(),
    loadedInitially: true,
    ownSession,
    lastMeditationSession
  };
}

// Helper functions
function sortMeditations(a: any, b: any) {
  return moment(b.createdAt).unix() - moment(a.createdAt).unix();
}

// Selectors for easy access
export const selectMeditations = (state: AppState) => state.meditations;
export const selectLoading = createSelector(selectMeditations, (state: MeditationState) => state.loading);
export const selectPosting = createSelector(selectMeditations, (state: MeditationState) => state.posting);
export const selectLiking = createSelector(selectMeditations, (state: MeditationState) => state.liking);
export const selectFinished = createSelector(selectMeditations, (state: MeditationState) => state.finished);
export const selectActive = createSelector(selectMeditations, (state: MeditationState) => state.active);
export const selectLastUpdated = createSelector(selectMeditations, (state: MeditationState) => state.lastUpdated);
export const selectLastMeditationSession = createSelector(selectMeditations, (state: MeditationState) => state.lastMeditationSession);
export const selectInitiallyLoaded = createSelector(selectMeditations, (state: MeditationState) => state.loadedInitially);
export const selectOwnSession = createSelector(selectMeditations, (state: MeditationState) => state.ownSession);
export const selectIsMeditating = createSelector(selectMeditations, (state: MeditationState) => !!state.ownSession);

