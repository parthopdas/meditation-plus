import { Component, Output, EventEmitter, ViewChild } from '@angular/core';
import { MeditationService } from '../../meditation/meditation.service';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

/**
 * Component for logging offline meditations
 */
@Component({
  selector: 'offline-meditation',
  templateUrl: './offline-meditation.html',
  styleUrls: [
    './offline-meditation.styl'
  ]
})
export class OfflineMeditationComponent {

  @Output() reload = new EventEmitter();
  @ViewChild('form') medFor: NgForm;

  today: Date = new Date();

  walking = '';
  sitting = '';
  date: Date = new Date();
  time = moment().subtract(1, 'hour').format('HH:mm');

  error = '';
  sending = false;

  constructor(
    private meditationService: MeditationService,
    private snackbar: MatSnackBar
  ) {
  }

  clearFormData() {
    this.medFor.resetForm();
    this.walking = '';
    this.sitting = '';
    this.date = new Date();
    this.time = '';
    this.error = '';
  }

  checkTime() {
    return new RegExp('^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$').test(this.time);
  }

  sendMeditation(evt) {
    if (evt) {
      evt.preventDefault();
    }

    const walking = this.walking ? parseInt(this.walking, 10) : 0;
    const sitting = this.sitting ? parseInt(this.sitting, 10) : 0;

    if ((!walking && !sitting)) {
      return;
    }

    // specify exact time
    const timeSplit = this.time.split(':');
    this.date.setHours(parseInt(timeSplit[0], 10));
    this.date.setMinutes(parseInt(timeSplit[1], 10));

    // send data to server
    this.sending = true;
    this.meditationService.post(walking, sitting, this.date)
      .subscribe(() => {
        this.sending = false;
        this.reload.emit('event');
        this.snackbar.open('Your entry has been added.');
        this.clearFormData();
      }, (err) => {
        this.error = err.error.errMsg;
        this.sending = false;
      });
  }
}
