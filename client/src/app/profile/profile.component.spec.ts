import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileComponent } from './profile.component';
import { UserService } from '../user/user.service';
import { FakeUserService } from '../user/testing/fake-user.service';
import { MockComponent } from 'ng2-mock-component';
import { MaterialModule } from '../shared/material.module';
import { RouterTestingModule } from '@angular/router/testing';
import { LinkyModule } from 'angular-linky';
import { FlagComponent } from './flag/flag.component';
import { SharedModule } from '../shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        LinkyModule,
        RouterTestingModule,
        SharedModule,
        HttpClientTestingModule
      ],
      declarations: [
        ProfileComponent,
        FlagComponent,
        MockComponent({selector: 'offline-meditation'}),
        MockComponent({selector: 'badge', inputs: ['numberBadges']}),
        MockComponent({selector: 'profile-charts', inputs: ['data', 'user']})
      ],
      providers: [
        {provide: UserService, useClass: FakeUserService},
        {provide: Store, useClass: MockStore}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
