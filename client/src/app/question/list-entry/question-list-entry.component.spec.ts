import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QuestionListEntryComponent } from './question-list-entry.component';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Component, OnInit, DebugElement } from '@angular/core';
import { MaterialModule } from '../../shared/material.module';
import { MomentModule } from 'ngx-moment';
import { LinkyModule } from 'angular-linky';
import { EmojiModule } from '../../emoji/emoji.module';
import { QuestionService } from '../question.service';
import { FakeQuestionService } from '../testing/fake-question.service';
import { UserTextListModule } from 'app/user-text-list/user-text-list.module';
import { SharedModule } from '../../shared';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Store } from '@ngrx/store';
import { MockStore } from 'testing/mock.store';
import { BroadcastUrlPipe } from './broadcast-url.pipe';

@Component({
  template: `
    <question-list-entry [question]="question" [mode]="mode">
    </question-list-entry>`
})


class TestHostComponent implements OnInit {
  question: any;
  isAdmin: boolean;
  mode: string;

  public ngOnInit() {
    this.isAdmin = false;
    this.mode = 'unanswered';

    this.question = {
      '_id': '333',
      'updatedAt': '2017-06-27T01:28:23.810Z',
      'createdAt': '2017-06-27T01:28:23.810Z',
      'text': 'question 1 \n',
      'user': {
        '_id': '444',
        'gravatarHash': 'hash',
        'name': 'Adam',
        'username': 'adam'
      },
      'likes': [],
      '__v': 0
    };
  }
}

describe('QuestionListEntryComponent', () => {
  let fixture: ComponentFixture<TestHostComponent>;
  let el: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        MomentModule,
        RouterTestingModule,
        LinkyModule,
        EmojiModule,
        UserTextListModule,
        SharedModule,
        HttpClientTestingModule
      ],
      declarations: [
        QuestionListEntryComponent,
        BroadcastUrlPipe,
        TestHostComponent
      ],
      providers: [
        {provide: QuestionService, useClass: FakeQuestionService},
        {provide: Store, useClass: MockStore}
      ]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    fixture.detectChanges();
    el = fixture.debugElement.query(By.css('question-list-entry'));
  });

  it('should be created', () => {
    expect(el).toBeTruthy();
  });
});
