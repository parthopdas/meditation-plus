export interface Question {
  _id?: string;
  text: string;
  user?: any;
  broadcast?: any;
  answered?: boolean;
  answeringAt?: Date;
  answeredAt?: Date;
  likes?: any[];
  numOfLikes?: number;
  videoUrl?: string;
}
