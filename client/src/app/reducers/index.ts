import {
  ActionReducerMap, MetaReducer, ActionReducer
} from '@ngrx/store';
import { MessageState, messageReducer } from '../message/reducers/message.reducers';
import { environment } from 'environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';
import { AuthState, authReducer } from '../auth/reducders/auth.reducers';
import { GlobalState, globalReducer } from './global.reducers';
import { localStorageSync } from 'ngrx-store-localstorage';
import { MeditationState, meditationReducer } from '../meditation/reducers/meditation.reducers';
import { QuestionState, questionReducer } from '../question/reducers/question.reducer';
import { AppointmentState, appointmentReducer } from '../appointment/reducers/appointment.reducer';

export interface AppState {
  messages: MessageState;
  meditations: MeditationState;
  questions: QuestionState;
  appointment: AppointmentState;
  auth: AuthState;
  global: GlobalState;
}

export const appReducers: ActionReducerMap<AppState> = {
  messages: messageReducer,
  meditations: meditationReducer,
  questions: questionReducer,
  appointment: appointmentReducer,
  auth: authReducer,
  global: globalReducer
};

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({
    keys: [{auth: ['token', 'profile', 'tokenExpires']}],
    rehydrate: true
  })(reducer);
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production
  ? [storeFreeze, localStorageSyncReducer]
  : [localStorageSyncReducer];
