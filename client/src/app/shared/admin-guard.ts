import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanLoad,
  Route
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectToken, selectTokenExpired, selectAdmin } from '../auth/reducders/auth.reducers';
import { withLatestFrom, map, tap, take } from 'rxjs/operators';

@Injectable()
export class AdminGuard implements CanActivate, CanLoad {
  constructor(
    private router: Router,
    private store: Store<AppState>
  ) {}

  check() {
    return this.store.select(selectToken).pipe(
      take(1),
      withLatestFrom(this.store.select(selectTokenExpired)),
      withLatestFrom(this.store.select(selectAdmin)),
      map(([[token, expired], admin]) => token && !expired && admin),
      tap(result => {
        if (!result) {
          this.router.navigate(['login']);
        }
      })
    );
  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    return this.check();
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.check();
  }
}
