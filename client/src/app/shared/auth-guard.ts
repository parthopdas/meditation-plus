import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanLoad,
  Route
} from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import { selectToken, selectTokenExpired } from '../auth/reducders/auth.reducers';
import { withLatestFrom, map, tap, take } from 'rxjs/operators';


@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private router: Router, private store: Store<AppState>) {}

  check() {
    return this.store.select(selectToken).pipe(
      take(1),
      withLatestFrom(this.store.select(selectTokenExpired)),
      map(([token, expired]) => token && !expired),
      tap(result => {
        if (!result) {
          this.router.navigate(['login']);
        }
      })
    );
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.check();
  }

  canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
    return this.check();
  }
}
