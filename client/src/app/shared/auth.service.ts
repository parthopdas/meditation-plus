import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiConfig } from 'api.config';
import { Injectable } from '@angular/core';
import { Profile } from 'app/profile/profile';

interface LoginResponse {
  token: string;
  profile: Profile;
}

@Injectable()
export class AuthService {
  public static tokenName = 'token';
  public static adminRole = 'ROLE_ADMIN';

  public constructor(private http: HttpClient) {
  }

  /**
   * Logging in via email and password.
   */
  public login(
    email: string,
    password: string,
    username?: string,
    acceptGdpr?: boolean,
    deleteAccount?: boolean
  ): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(
      ApiConfig.url + '/auth/login',
      JSON.stringify({ email, password, username, acceptGdpr, deleteAccount })
    );
  }

  /**
   * Verify account by sending token received via email to server
   */
  public verify(token: string): Observable<any> {
    return this.http.post<any>(
      ApiConfig.url + '/auth/verify',
      JSON.stringify({ token })
    );
  }

  /**
   * Resend email activation with token
   * @param email mail address of user
   */
  public resend(email: string): Observable<any> {
    return this.http.post<any>(
      ApiConfig.url + '/auth/resend',
      JSON.stringify({ email })
    );
  }

  public resetPasswordInit(email: string): Observable<any> {
    return this.http.post<any>(
      ApiConfig.url + '/auth/reset-init',
      JSON.stringify({ email })
    );
  }

  public resetPassword(userId: string, token: string, newPassword: string): Observable<any> {
    return this.http.post<any>(
      ApiConfig.url + '/auth/reset',
      JSON.stringify({ userId, token, newPassword })
    );
  }

  public signup(name: string, password: string, email: string, username: string, acceptedGdpr: boolean): Observable<any> {
    return this.http.post(
      ApiConfig.url + '/auth/signup',
      JSON.stringify({name, password, email, username, acceptedGdpr})
    );
  }

  /**
   * Logging out the current user. Removes the token from localStorage.
   */
  public logout(): Observable<any> {
    const token = localStorage.getItem('MEDITATION_PLUS_FCM_TOKEN');
    return this.http.post(
      ApiConfig.url + '/auth/logout',
      JSON.stringify({ token })
    );
  }
}
