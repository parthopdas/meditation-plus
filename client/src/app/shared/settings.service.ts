import { Injectable } from '@angular/core';
import { ApiConfig } from '../../api.config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class SettingsService {

  public constructor(
    private http: HttpClient
  ) {
  }

  public get(): Observable<any> {
    return this.http.get(ApiConfig.url + '/api/settings');
  }

  public set(property: string, value: any): Observable<any> {
    return this.http.put(ApiConfig.url + '/api/settings/' + property, { value });
  }
}
