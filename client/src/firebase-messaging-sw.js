// * Here is is the code snippet to initialize Firebase Messaging in the Service
// * Worker when your app is not hosted on Firebase Hosting.
// [START initialize_firebase_in_sw]
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
 'messagingSenderId': '890265754427'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
// [END initialize_firebase_in_sw]


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);

  // Customize notification here
  var notificationTitle = payload.data && payload.data.title
    ? payload.data.title
    : 'Meditation+';

  if (!payload.data) {
    payload.data = {};
  }

  // set a default icon if none was declared
  if (!payload.data.icon) {
    payload.data.icon = './assets/icon/android-chrome-192x192.png';
  }

  // parse custom data
  if (typeof payload.data.data === 'string') {
    payload.data.data = JSON.parse(payload.data.data);
  }

  return self.registration.showNotification(notificationTitle,
    payload.data);
});
// [END background_handler]
