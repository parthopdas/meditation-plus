import { of } from 'rxjs';

export class MockActions {
  ofType(type) { return of(); }
}
