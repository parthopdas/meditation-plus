import Settings from '../models/settings.model.js';
import Appointment from '../models/appointment.model.js';
import moment from 'moment-timezone';
import push from './push.js';
import config from '../../config/config.json';
import { round } from 'lodash';

/**
 * Parse appointment hour stored as number
 * into hour and minute.
 */
const parseAppointmentHour = (rawHour) => ({
  hour: Math.floor(rawHour / 100),
  minute: rawHour % 100
});

/**
 * Calculates the minutes remaining minutes to the appointment
 * of the current week.
 *
 * @param {Appointment} appointment
 *
 * @return difference in minutes to the appointment.
 */
const minutesUntilAppointment = async (appointment, nowMoment = moment.utc()) => {
  if (!appointment || typeof appointment.hour !== 'number' || typeof appointment.weekDay !== 'number') {
    return Infinity;
  }

  // load settings entity
  const settings = await Settings.findOne();
  const appointmentsTimezone = settings && !!moment.tz.zone(settings.appointmentsTimezone)
    ? settings.appointmentsTimezone
    : '';

  const nowMomentTz = moment.tz(nowMoment, appointmentsTimezone);

  // create a Moment for appointment
  const { hour, minute } = parseAppointmentHour(appointment.hour);
  let appDateTimeSpecs = {
    weekday: appointment.weekDay,
    hour,
    minute,
    second: 0,
    millisecond: 0
  };

  // treat special cases which occur betwen SATURDAY and SUNDAY
  // because of the indexing from 0-6
  if (nowMomentTz.weekday() === 6 && appointment.weekDay === 0) {
    appDateTimeSpecs.weekday = 7; // now = SATURDAY, appointment = SUNDAY => take tomorror (0 + 7)
  } else if (nowMomentTz.weekday() === 0 && appointment.weekDay === 6) {
    appDateTimeSpecs.weekday = -1; // now = SUNDAY, appointment = SATURDAY => take yesterday (6 - 7)
  }

  const appMoment = nowMomentTz.clone().set(appDateTimeSpecs);
  return round(appMoment.diff(nowMomentTz, 'minutes', true), 2);
};

/**
 * Find an appointment that is scheduled for now for a given user.
 *
 * @param {User}    user               The user to make the lookup for
 * @param {number}  tooEarlyTolerance  Number of minutes before the appointment's scheduled time
 *                                     tolerated for user to initiate.
 * @param {number}  tooLateTolerance   Number of minutes after the appointment's scheduled time
 *                                     tolerated for user to initiate.
 *
 * @return A matching appointment object or null.
 */
const findCurrentAppointmentForUser = async (user, tooEarlyTolerance = 5, tooLateTolerance = 7) => {
  if (!user || !user._id) {
    return null;
  }

  let appointment = await Appointment
    .findOne({ user: user._id })
    .lean();

  const diffMins = await minutesUntilAppointment(appointment);
  return -1 * diffMins <= tooLateTolerance && diffMins <= tooEarlyTolerance
    ? appointment
    : null;
};

/**
 * Sends out the ringing for a freshly initiated meeting.
 */
const sendAppointmentNotificationToBhante = async (meeting) => await push.sendToTopic(`appointments-${config.MESSAGING_APPOINTMENT_TOPIC}`, {
  android: {
    data: {
      body: `${meeting.user.name} (@${meeting.user.username}) tries to reach you out on Discord.`,
    },
    priority: 'high'
  }
});

export {
  parseAppointmentHour,
  minutesUntilAppointment,
  findCurrentAppointmentForUser,
  sendAppointmentNotificationToBhante
};
