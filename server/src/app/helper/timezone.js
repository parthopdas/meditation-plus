import moment from 'moment-timezone';

/**
* Method for converting times to user's timezone
*/
export default (user, time) => {
  if (!user.timezone || !moment.tz.zone(user.timezone)) {
    return moment(time).utc();
  }

  return moment(time).tz(user.timezone);
};
