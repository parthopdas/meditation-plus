import { google } from 'googleapis';
import { logger } from '../helper/logger.js';

export default () => {
  const youtube = google.youtube({
    version: 'v3',
    auth: process.env.GOOGLE_API_KEY
  });

  return {
    getLivestreamInfo: async () => {
      try {
        const res = await youtube.search.list({
          part: 'snippet',
          channelId: 'UCQJ6ESCWQotBwtJm0Ff_gyQ',
          type: 'video',
          eventType: 'live'
        });

        return res.data;
      } catch (err) {
        logger.error('YOUTUBE ERROR', err);
      }

      return null;
    },
    findMatchingVideos: async (query, limit = 10) => {
      try {
        const res = await youtube.search.list({
          part: 'snippet',
          channelId: 'UCQJ6ESCWQotBwtJm0Ff_gyQ',
          maxResults: limit,
          q: query
        });

        return res.data;
      } catch (err) {
        logger.error('YOUTUBE ERROR', err);
      }

      return null;
    },
    findBroadcastByTime: async (publishedAfter, publishedBefore) => {
      try {
        const res = await youtube.search.list({
          part: 'id',
          channelId: 'UCQJ6ESCWQotBwtJm0Ff_gyQ',
          type: 'video',
          eventType: 'completed',
          order: 'date',
          publishedBefore: publishedBefore,
          publishedAfter: publishedAfter
        });

        // reverse order
        if (res.data && res.data.items) {
          res.data.items = res.data.items.reverse();
        }

        return res.data;
      } catch (err) {
        logger.error('YOUTUBE ERROR', err);
      }

      return null;
    },
    getLiveStreamDetails: async (videoId) => {
      try {
        const res = await youtube.videos.list({
          part: 'liveStreamingDetails',
          id: videoId
        });

        return res.data;
      } catch (err) {
        logger.error('YOUTUBE ERROR', err);
      }

      return null;
    }
  };
};
