import mongoose from 'mongoose';

let MeetingSchema = mongoose.Schema({
  user: {
    type : mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  appointment: {
    type : mongoose.Schema.Types.ObjectId,
    ref: 'Appointment',
    required: true
  },
  initiated: { type: Boolean, default: false, required: true },
  discordLink: { type: String }
}, {
  timestamps: true
});

export default mongoose.model('Meeting', MeetingSchema);
