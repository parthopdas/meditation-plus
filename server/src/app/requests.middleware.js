import { User } from './models/user.model.js';

export default async function(req, res, next) {
  // update lastActive for user
  if (req.user) {
    let user = await User.findById(req.user._id);

    if (user) {
      if (user.suspendedUntil && user.suspendedUntil > new Date()) {
        res.sendStatus(401);
      }

      if (!user.username) {
        req.logOut();
        res
          .status(401)
          .json({ message: 'missing username' });
      }

      if (!user.acceptedGdpr) {
        req.logOut();
        res
          .status(401)
          .json({ message: 'missing gdpr' });
      }

      user.lastActive = new Date();
      await user.save();
    }
  }

  // Make sure we go to the next routes and don't stop here...
  next();
}
