import Appointment from '../models/appointment.model.js';
import Meeting from '../models/meeting.model.js';
import Settings from '../models/settings.model.js';
import { logger } from '../helper/logger.js';
import { discord } from '../helper/discord.js';
import { findCurrentAppointmentForUser, sendAppointmentNotificationToBhante } from '../helper/appointment.js';
import moment from 'moment-timezone';
import { get as gv } from 'lodash';

export default (app, router, io, admin) => {

  /**
   * @api {get} /api/appointment Get appointment data
   * @apiName ListAppointment
   * @apiGroup appointment
   *
   * @apiSuccess {Object[]} appointments             List of appointment slots
   * @apiSuccess {Number}   appointments.hour        Hour of day
   * @apiSuccess {Number}   appointments.weekDay     Number of week day
   * @apiSuccess {Object}   appointments.user        The meditating User
   */
  router.get('/api/appointment', async (req, res) => {
    try {
      let json = {
        ownAppointment: null,
        appointments: []
      };

      const result = await Appointment
        .find()
        .sort({
          weekDay: 'asc',
          hour: 'asc'
        })
        .populate('user', 'name avatarImageToken username')
        .lean()
        .exec();

      const settings = await Settings.findOne();
      const now = moment.tz(settings.appointmentsTimezone || '');

      json.appointments = result.map(entry => {
        let nextDate = now.clone().set({
          weekday: entry.weekDay,
          hour: Math.floor(entry.hour / 100),
          minute: entry.hour % 100,
          second: 0
        });

        if (nextDate < now) {
          nextDate = nextDate.add(7, 'days');
        }

        return {
          nextDate: nextDate.format(),
          ...entry
        };
      });

      json.ownAppointment = json.appointments.find((appointment) =>
        gv(appointment, 'user._id', '').toString() === req.user._id.toString()
      ) || null;

      res.json(json);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });

  /**
   * @api {get} /api/appointment/meeting Get appointment meeting scheduled for now.
   * @apiName GetAppointmentMeeting
   * @apiGroup appointment
   *
   * @apiSuccess {Object[]} meeting               The meeting object
   * @apiSuccess {ObjectId} meeting.user          User who has booked appointment
   * @apiSuccess {ObjectId} meeting.appointment   Appointment which was booked by user
   * @apiSuccess {Boolean}  meeting.initiated     Whether /api/appointment/initiate was already called
   * @apiSuccess {String}   meeting.discordLink   Access link to discord channel for meeting
   */
  router.get('/api/appointment/meeting', async (req, res) => {
    try {
      const ongoingAppointment = await findCurrentAppointmentForUser(req.user, 5, 25);

      if (!ongoingAppointment) {
        return res.status(403).json({ error: 'Unauthorized' });
      }

      let existingMeeting = await Meeting.findOne({
        user: req.user._id,
        appointment: ongoingAppointment._id,
        createdAt: { $gte: moment().subtract(30, 'minutes') }
      });

      if (existingMeeting) {
        // fill up meeting with discord link if missing
        if (!existingMeeting.discordLink) {
          let discordLink = '';

          try {
            discordLink = await discord.createAppointmentInviteLink();
            await Meeting.findOneAndUpdate({ _id: existingMeeting._id }, { discordLink });
            existingMeeting = {
              ...existingMeeting,
              discordLink
            };
          } catch (someErr) {
            logger.error(req.url, someErr);
          }
        }

        return res.status(200).json(existingMeeting);
      }

      // try to create new meeting if there is no yet
      // and it's not too late
      const appointment = await findCurrentAppointmentForUser(req.user, 5, 10);

      if (!appointment) {
        return res.status(403).json({ error: 'Unauthorized' });
      }

      let discordLink = '';
      try {
        discordLink = await discord.createAppointmentInviteLink();
      } catch (discordErr) {
        logger.error(`${req.url} [DISCORD ERROR]`, discordErr);
      }

      const newMeeting = await Meeting.create({
        user: req.user._id,
        appointment: appointment._id,
        initiated: false,
        discordLink
      });

      res.status(201).json(newMeeting);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });

  /**
   * @api {post} /api/appointment/meeting-notify Trigger meeting notification for services without ringtone.
   * @apiName TriggerMeetingNotification
   * @apiGroup appointment
   */
  router.post('/api/appointment/meeting-notify', async (req, res) => {
    try {
      const ongoingAppointment = await findCurrentAppointmentForUser(req.user, 5, 10);

      if (!ongoingAppointment) {
        return res.status(403).json({
          error: 'This action is restricted to the time range from 5 minutes before until 10 minutes after the appointment is scheduled.'
        });
      }

      const ongoingMeeting = await Meeting
        .findOne({
          user: req.user._id,
          appointment: ongoingAppointment._id,
          initiated: false,
          createdAt: { $gte: moment().subtract(20, 'minutes') }
        })
        .populate('user', 'name username');

      if (!ongoingMeeting) {
        return res.status(403).json({
          error: 'Meeting does either not exist or is already initiated.'
        });
      }

      const success = await sendAppointmentNotificationToBhante(ongoingMeeting);

      if (!success) {
        return res.status(400).json({
          error: 'It appears that the sending of the notification failed. Please retry.'
        });
      }

      await ongoingMeeting.update({ initiated: true });
      res.status(204).json();
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  });

  /**
   * @api {get} /api/appointment/aggregated Get appointment days aggregated by their hour
   * @apiName AggregateAppointment
   * @apiGroup appointment
   */
  router.get('/api/appointment/aggregated', async (req, res) => {
    try {
      const data = await Appointment.aggregate([
        {
          $group: {
            _id: '$hour',
            days: { $push: '$weekDay'}
          }
        },
        {
          $sort: { _id: 1 }
        }
      ]);

      res.json(data);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {get} /api/appointment/:id Get single appointment
   * @apiName GetAppointment
   * @apiGroup Appointment
   *
   * @apiSuccess {Number}   weekDay        1 to 7
   * @apiSuccess {Number}   hour           UTC hour
   */
  router.get('/api/appointment/:id', admin, async (req, res) => {
    try {
      let result = await Appointment
        .findOne({ _id: req.params.id })
        .lean();

      if (!result) return res.sendStatus(404);

      res.json(result);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });

  /**
   * @api {post} /api/appointment/update Update the hour of all appointments with a specific hour
   * @apiName UpdateAppointments
   * @apiGroup Appointment
   *
   * @apiParam {number} oldHour   Hour/Time of appointments to be changed from
   * @apiParam {number} newHour   Hour/Time of appointments to be changed to
   */
  router.post('/api/appointment/update', admin, async (req, res) => {
    try {
      if (typeof req.body['oldHour'] !== 'number' || typeof req.body['newHour'] !== 'number') {
        return res.status(400).send('Invalid Parameters.');
      }

      const duplicates = await Appointment
        .find({
          hour: req.body['newHour']
        })
        .lean();

      if (duplicates.length > 0) {
        return res.status(400).send('Appointments with this hour already exist.');
      }

      const appointments = await Appointment
        .find({
          hour: req.body['oldHour']
        });

      if (appointments.length === 0) {
        return res.status(400).send('No appointments with this hour found.');
      }

      for (let appoint of appointments) {
        await appoint.update({
          hour: req.body['newHour']
        });
      }

      io.sockets.emit('appointment', true);

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {post} /api/appointment/toggle Toggle an appointment (create or delete it) based on hour and day
   * @apiName ToggleAppointment
   * @apiGroup Appointment
   *
   * @apiParam {number} hour   Hour of appointment
   * @apiParam {number} day    Weekday of appointment
   */
  router.post('/api/appointment/toggle', admin, async (req, res) => {
    try {
      if (typeof req.body['hour'] !== 'number' || typeof req.body['day'] !== 'number') {
        return res.status(400).send('Invalid Parameters.');
      }

      const appointments = await Appointment
        .find({
          hour: req.body['hour'],
          ...(req.body['day'] !== -1 && { weekDay: req.body['day'] })
        })
        .exec();

      if (appointments.length > 0) {
        // toggle = delete
        for (const appoint of appointments) {
          await appoint.remove();
        }
      } else {
        // toggle = create
        await Appointment.create({
          hour: req.body['hour'],
          weekDay: req.body['day']
        });
      }

      io.sockets.emit('appointment', true);
      res.sendStatus(appointments.length > 0 ? 204 : 201);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {post} /api/appointment Add new appointment
   * @apiName AddAppointment
   * @apiGroup Appointment
   */
  router.post('/api/appointment', admin, async (req, res) => {
    try {
      // check for duplicates
      const duplicates = await Appointment
        .find({
          weekDay: req.body.weekDay,
          hour: req.body.hour
        })
        .lean();

      if (duplicates.length > 0) {
        return res.status(400).send('This appointment already exists.');
      }

      await Appointment.create({
        weekDay: req.body.weekDay,
        hour: req.body.hour
      });

      res.sendStatus(201);
    } catch (err) {
      logger.error(req.url, err);
      res
        .status(err.name === 'ValidationError' ? 400 : 500)
        .send(err);
    }
  });

  /**
   * @api {post} /api/appointment Toggle registration to appointment
   * @apiName ToggleAppointmentRegistration
   * @apiGroup appointment
   *
   * @apiParam {String} id Appointment ID
   */
  router.post('/api/appointment/:id/register', async (req, res) => {
    try {
      // gets appointment
      let appointment = await Appointment
        .findById(req.params.id)
        .exec();

      if (!appointment) return res.sendStatus(404);

      // check if another user is registered
      if (appointment.user && appointment.user != req.user._id) {
        return res.status(400).send('another user is registered');
      }

      if (!appointment.user) {
        // check if user is already in another appointment and remove it
        const otherAppointment = await Appointment
          .findOne({
            user: req.user._id
          })
          .exec();

        if (otherAppointment) {
          otherAppointment.user = null;
          await otherAppointment.save();
        }
      }

      // toggle registration for current user
      appointment.user = appointment.user && appointment.user == req.user._id
        ? null
        : req.user;

      await appointment.save();
      // sending broadcast WebSocket for taken/fred appointment
      io.sockets.emit('appointment', appointment);

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(400).send(err);
    }
  });

  /**
   * @api {delete} /api/appointment/remove/:id Deletes any user from appointment
   * @apiName DeleteRegistration
   * @apiGroup Appointment
   */
  router.delete('/api/appointment/remove/:id', admin, async (req, res) => {
    try {
      // gets appointment
      let appointment = await Appointment
        .findById(req.params.id)
        .exec();

      if (!appointment) return res.sendStatus(404);

      // Remove registration from appointment
      appointment.user = null;

      await appointment.save();
      // sending broadcast WebSocket for taken/fred appointment
      io.sockets.emit('appointment', appointment);

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.send(err);
    }
  });
};
