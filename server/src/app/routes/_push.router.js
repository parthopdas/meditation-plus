import { User } from '../models/user.model.js';
import FCMSubscription from '../models/fcm.model.js';
import { logger } from '../helper/logger.js';

export default (app, router) => {
  /**
   * @api {post} /api/push/register Register a new push subscription for FCM
   * @apiName SubscribePush
   * @apiGroup Push
   *
   * @apiParam {String} token
   */
  router.post(['/api/push/register_fcm', '/api/push/register'], async (req, res) => {
    try {
      const token = req.body.token ? req.body.token : null;
      if (!token) return res.sendStatus(400);

      const user = await User.findOne({ _id : req.user._id });
      if (!user) return res.sendStatus(403);

      const oldToken = req.body.oldToken ? req.body.oldToken : null;
      await FCMSubscription.findOneAndUpdate({ token: oldToken ? oldToken : token }, {
        user: user._id,
        token
      }, {
        new: true,
        upsert: true,
        setDefaultsOnInsert: true
      });

      res.sendStatus(204);
    } catch (err) {
      logger.error(req.url, err);
      res.status(500).send(err);
    }
  });
};
